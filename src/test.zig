const builtin = @import("builtin");
const std = @import("std");
const testing = std.testing;

const sound = @import("zig-sound");

test "pcm playback" {
    var device: ?*sound.snd_pcm_t = null;
    _ = try sound.checkError(sound.snd_pcm_open(
        &device,
        "default",
        sound.snd_pcm_stream_t.PLAYBACK,
        0,
    ));

    var hw_params: ?*sound.snd_pcm_hw_params_t = null;
    _ = try sound.checkError(sound.snd_pcm_hw_params_malloc(&hw_params));
    defer sound.snd_pcm_hw_params_free(hw_params);

    _ = try sound.checkError(sound.snd_pcm_hw_params_any(device, hw_params));

    _ = try sound.checkError(sound.snd_pcm_hw_params_set_rate_resample(
        device,
        hw_params,
        1,
    ));

    _ = try sound.checkError(sound.snd_pcm_hw_params_set_access(
        device,
        hw_params,
        sound.snd_pcm_access_t.RW_INTERLEAVED,
    ));

    _ = try sound.checkError(sound.snd_pcm_hw_params_set_format(
        device,
        hw_params,
        switch (builtin.target.cpu.arch.endian()) {
            .Little => sound.snd_pcm_format_t.FLOAT_LE,
            .Big => sound.snd_pcm_format_t.FLOAT_BE,
        },
    ));

    const num_channels = 2;
    _ = try sound.checkError(sound.snd_pcm_hw_params_set_channels(
        device,
        hw_params,
        num_channels,
    ));

    var sample_rate: c_uint = 44100;
    _ = try sound.checkError(sound.snd_pcm_hw_params_set_rate_near(
        device,
        hw_params,
        &sample_rate,
        null,
    ));

    _ = try sound.checkError(sound.snd_pcm_hw_params(device, hw_params));

    var buffer_frames: sound.snd_pcm_uframes_t = undefined;
    _ = try sound.checkError(sound.snd_pcm_hw_params_get_buffer_size(
        hw_params,
        &buffer_frames,
    ));

    var buffer = try std.testing.allocator.alloc(f32, buffer_frames * num_channels);
    defer std.testing.allocator.free(buffer);

    const pitch: f32 = 261.63;
    const radians_per_sec = pitch * 2 * std.math.pi;
    const sec_per_frame = 1 / @as(f32, @floatFromInt(sample_rate));

    _ = try sound.checkError(sound.snd_pcm_prepare(device));

    var frame: usize = 0;
    while (frame < buffer_frames) : (frame += 1) {
        const s = std.math.sin((@as(f32, @floatFromInt(frame)) * sec_per_frame) * radians_per_sec);
        var channel: usize = 0;
        while (channel < num_channels) : (channel += 1) {
            buffer[frame * num_channels + channel] = s;
        }
    }

    std.log.warn(
        "playing {} seconds of samples...",
        .{buffer_frames / sample_rate},
    );

    if (sound.snd_pcm_writei(
        device,
        @as(*anyopaque, @ptrCast(buffer)),
        buffer_frames,
    ) < 0) {
        _ = try sound.checkError(sound.snd_pcm_prepare(device));
    }

    _ = try sound.checkError(sound.snd_pcm_drain(device));

    _ = try sound.checkError(sound.snd_pcm_close(device));
}
