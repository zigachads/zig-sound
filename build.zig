const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const module = b.addModule("zig-sound", .{
        .source_file = .{ .path = "./src/main.zig" },
        .dependencies = &.{},
    });

    const main_tests = b.addTest(.{
        .root_source_file = .{ .path = "./src/test.zig" },
        .target = target,
        .optimize = optimize,
    });
    main_tests.linkLibC();
    main_tests.addModule("zig-sound", module);
    main_tests.linkSystemLibrary("asound");

    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&main_tests.step);
}
